#! /bin/bash

set -ex

yum -y install tar

cd /usr/local/src
curl -L https://storage.googleapis.com/golang/go1.5.3.linux-amd64.tar.gz -o go1.5.3.linux-amd64.tar.gz
tar zxvf go1.5.3.linux-amd64.tar.gz

ln -s /usr/local/src/go /usr/local/go

echo "export GOROOT=/usr/local/go" >> /etc/profile.d/go.sh
echo "export GOPATH=\$GOROOT/extend-pkg" >> /etc/profile.d/go.sh
echo "export PATH=\$PATH:\$GOROOT/bin" >> /etc/profile.d/go.sh
echo "export PATH=\$PATH:\$GOROOT/bin:\$GOPATH/bin" >> /etc/profile.d/go.sh
